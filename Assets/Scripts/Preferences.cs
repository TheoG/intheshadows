﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Preferences {

	//public static Preferences options = null;
	
	public const float defaultSensibility = 5f;
	public const float defaultMusicVolume = 80f;
	public const float defaultSfxVolume = 80f;

	public const string sensibilityIndex = "SENSIBILITY";
	public const string musicVolumeIndex = "MUSIC_VOLUME";
	public const string sfxVolumeIndex = "SFX_VOLUME";

	public static Dictionary<string, float> values = new Dictionary<string, float>();

	static Preferences() {
		values[sensibilityIndex] = GetFloat(sensibilityIndex);
		if (values[sensibilityIndex] == -1f) {
			values[sensibilityIndex] = defaultSensibility;
			SetFloat(sensibilityIndex, values[sensibilityIndex]);
		}

		values[musicVolumeIndex] = GetFloat(musicVolumeIndex);
		if (values[musicVolumeIndex] == -1f) {
			values[musicVolumeIndex] = defaultMusicVolume;
			SetFloat(musicVolumeIndex, values[musicVolumeIndex]);
		}

		values[sfxVolumeIndex] = GetFloat(sfxVolumeIndex);
		if (values[sfxVolumeIndex] == -1f) {
			values[sfxVolumeIndex] = defaultSfxVolume;
			SetFloat(sfxVolumeIndex, values[sfxVolumeIndex]);
		}
	}

	public static void SetFloat(string index, float value) {
		PlayerPrefs.SetFloat(index, value);
		values[index] = value;
	}

	public static float GetFloat(string index) {
		return PlayerPrefs.GetFloat(index, -1f);
	}
}
