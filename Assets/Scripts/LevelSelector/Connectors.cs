﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Connectors : MonoBehaviour {

	public GameObject dash;
	public GameObject dashContainer;
	public GameObject cam;
	public float spacing = 0.5f;
	public GameObject winningParticles;

	[System.Serializable]
	public class LevelMenu {
		public Transform transform;
		public string name;
		public List<string> nextLevelNames;
	}

	[System.Serializable]
	public class ListLevel {
		public List<LevelMenu> levels;
	}

	public ListLevel listLevel;

	private LevelSelectorCamera levelSelectorCamera;

	private Transform childTransform;
	private Transform nextTransform;
	private float width;
	private ParticleSystem winningParticleSystem;

	private Dictionary<string, LevelMenu> nameLevelHash = new Dictionary<string, LevelMenu>();

	void Start() {

		width = dash.GetComponent<Renderer>().bounds.size.x;

		if (cam == null) {
			cam = GameObject.Find("Main Camera");
		}
		levelSelectorCamera = cam.GetComponent<LevelSelectorCamera>();
		winningParticleSystem = winningParticles.GetComponent<ParticleSystem>();

		// Move in the Level Script
		foreach (LevelMenu level in listLevel.levels) {
			nameLevelHash.Add(level.name, level);
			TextMeshPro[] txtmp = level.transform.GetComponentsInChildren<TextMeshPro>();
			txtmp[1].SetText(level.transform.name.Replace('_', ' '));
			txtmp[2].SetText(GameManager.instance.savedLevelHashMap[level.name].GetHint());
		}


		foreach (LevelMenu level in listLevel.levels) {

			childTransform = level.transform;
			level.transform.GetComponent<Level>().SetState(GameManager.instance.cheatMode ? Level.Status.Unlocked : GameManager.instance.savedLevelHashMap[level.name].GetStatus());
			foreach (string nextLevelName in level.nextLevelNames) {
				if (GameManager.instance.savedLevelHashMap[nextLevelName].GetStatus() == Level.Status.Locked && !GameManager.instance.cheatMode) {
					continue;
				}


				nextTransform = nameLevelHash[nextLevelName].transform;
				// TODO: check if level not already unlocked (children already unlocked for example)
				if (GameManager.lastUnlockedLevel != null && level.name == GameManager.lastUnlockedLevel.GetName()) {
					if (childTransform.position.y == nextTransform.position.y) {
						StartCoroutine(DrawLinearPathAnimation());
					} else {
						StartCoroutine(DrawLevelUpPathAnimation());
					}
				} else {
					if (childTransform.position.y == nextTransform.position.y) {
						DrawLinearPath();
					}
					else {
						DrawLevelUpPath();
					}
				}
			}
		}
		if (GameManager.lastUnlockedLevel != null) {
			Vector3 camPosition = nameLevelHash[GameManager.lastUnlockedLevel.GetName()].transform.position;
			camPosition.z = cam.transform.position.z;
			cam.transform.position = camPosition;
			levelSelectorCamera.SetTarget(camPosition);
			camPosition.z = winningParticles.transform.position.z;
			winningParticles.transform.position = camPosition;
			winningParticleSystem.Emit(300);
			SoundManager.instance.WinningParticle();
		}
	}

	IEnumerator DrawLinearPathAnimation() {
		float delta = nextTransform.position.x - childTransform.position.x - 2f; //1f == center to edge * 2
		int nb = (int)(delta / (spacing + width));
		float remainingSpace = delta - (nb * (spacing + width));
		Vector3 instantiationPosition = childTransform.GetChild(0).transform.position + new Vector3(1f + width / 2f + remainingSpace / 2f + spacing / 2f, 0f, 0f);
		yield return new WaitForSeconds(0.5f);
		Vector3 camPosition = Vector3.zero;
		levelSelectorCamera.followAnimation = true;
		for (int j = 0; j < nb; j++) {
			Vector3 spawnPoint = instantiationPosition;
			Collider[] hitColliders = Physics.OverlapSphere(spawnPoint, 0.001f); //1 is purely chosen arbitrarily
			if (hitColliders.Length == 0) {
				Instantiate(dash, instantiationPosition, dash.transform.rotation, dashContainer.transform);
			}

			camPosition.x = instantiationPosition.x;
			camPosition.y = instantiationPosition.y;
			camPosition.z = cam.transform.position.z;

			if (levelSelectorCamera.followAnimation) {
				levelSelectorCamera.SetTarget(camPosition);
			}

			instantiationPosition.x += spacing + width;
			yield return new WaitForSeconds(0.5f);
		}
		camPosition = nextTransform.position;
		camPosition.z = cam.transform.position.z;

		if (levelSelectorCamera.followAnimation) {
			levelSelectorCamera.SetTarget(camPosition);
			levelSelectorCamera.followAnimation = false;
		}
	}

	IEnumerator DrawLevelUpPathAnimation() {

		float xDelta = nextTransform.position.x - childTransform.position.x - 2f; //1f == center to edge * 2
		float yDelta = (nextTransform.position.y - childTransform.position.y) * 2f;
		int xNb = (int)(xDelta / (spacing + width));
		int yNb = (int)(yDelta / (spacing + width));
		int direction = yNb > 0 ? 1 : -1;
		yNb = Mathf.Abs(yNb);
		Vector3 camPosition = Vector3.zero;
		float xRemainingSpace = xDelta - (xNb * (spacing + width));
		Vector3 instantiationPosition = childTransform.GetChild(0).transform.position + new Vector3(1f + width + xRemainingSpace / 2f, 0f, 0f);
		for (int j = 0; j < xNb / 2; j++) {
			Vector3 spawnPoint = instantiationPosition;
			Collider[] hitColliders = Physics.OverlapSphere(spawnPoint, 0.001f); //1 is purely chosen arbitrarily
			if (hitColliders.Length == 0) {
				Instantiate(dash, instantiationPosition, dash.transform.rotation, dashContainer.transform);
			}

			camPosition.x = instantiationPosition.x;
			camPosition.y = instantiationPosition.y;
			camPosition.z = cam.transform.position.z;

			if (levelSelectorCamera.followAnimation) {
				levelSelectorCamera.SetTarget(camPosition);
			}

			instantiationPosition.x += spacing + width;
			yield return new WaitForSeconds(0.25f);
		}
		instantiationPosition.x -= (spacing + width) / 2f;
		instantiationPosition.y += width * direction;
		for (int j = 0; j < yNb / 2; j++) {
			GameObject go = Instantiate(dash, instantiationPosition, dash.transform.rotation, dashContainer.transform);
			go.transform.Rotate(transform.up, -90f);

			camPosition.x = instantiationPosition.x;
			camPosition.y = instantiationPosition.y;
			camPosition.z = cam.transform.position.z;

			if (levelSelectorCamera.followAnimation) {
				levelSelectorCamera.SetTarget(camPosition);
			}
			yield return new WaitForSeconds(0.25f);
			instantiationPosition.y += (spacing + width) * direction;
		}
		instantiationPosition.x += (spacing + width) / 2f;
		instantiationPosition.y = nextTransform.position.y;
		for (int j = 0; j < xNb / 2; j++) {

			Instantiate(dash, instantiationPosition, dash.transform.rotation, dashContainer.transform);

			camPosition.x = instantiationPosition.x;
			camPosition.y = instantiationPosition.y;
			camPosition.z = cam.transform.position.z;

			if (levelSelectorCamera.followAnimation) {
				levelSelectorCamera.SetTarget(camPosition);
			}
			yield return new WaitForSeconds(0.25f);
			instantiationPosition.x += spacing + width;
		}
		camPosition = nextTransform.position;
		camPosition.z = cam.transform.position.z;

		if (levelSelectorCamera.followAnimation) {
			levelSelectorCamera.SetTarget(camPosition);
			levelSelectorCamera.followAnimation = false;
		}
	}

	void DrawLinearPath() {
		float delta = nextTransform.position.x - childTransform.position.x - 2f; //1f == center to edge * 2
		int nb = (int)(delta / (spacing + width));
		float remainingSpace = delta - (nb * (spacing + width));
		Vector3 instantiationPosition = childTransform.GetChild(0).transform.position + new Vector3(1f + width / 2f + remainingSpace / 2f + spacing / 2f, 0f, 0f);
		for (int j = 0; j < nb; j++) {
			Vector3 spawnPoint = instantiationPosition;
			Collider[] hitColliders = Physics.OverlapSphere(spawnPoint, 0.001f); //1 is purely chosen arbitrarily
			if (hitColliders.Length == 0) {
				Instantiate(dash, instantiationPosition, dash.transform.rotation, dashContainer.transform);
			}
			instantiationPosition.x += spacing + width;
		}
	}

	void DrawLevelUpPath() {

		float xDelta = nextTransform.position.x - childTransform.position.x - 2f; //1f == center to edge * 2
		float yDelta = (nextTransform.position.y - childTransform.position.y) * 2f;
		int xNb = (int)(xDelta / (spacing + width));
		int yNb = (int)(yDelta / (spacing + width));
		int direction = yNb > 0 ? 1 : -1;
		yNb = Mathf.Abs(yNb);
		float xRemainingSpace = xDelta - (xNb * (spacing + width));
		Vector3 instantiationPosition = childTransform.GetChild(0).transform.position + new Vector3(1f + width + xRemainingSpace / 2f, 0f, 0f);
		for (int j = 0; j < xNb / 2; j++) {
			Vector3 spawnPoint = instantiationPosition;
			Collider[] hitColliders = Physics.OverlapSphere(spawnPoint, 0.001f); //1 is purely chosen arbitrarily
			if (hitColliders.Length == 0) {
				Instantiate(dash, instantiationPosition, dash.transform.rotation, dashContainer.transform);
			}
			instantiationPosition.x += spacing + width;
		}
		instantiationPosition.x -= (spacing + width) / 2f;
		instantiationPosition.y += width * direction;
		for (int j = 0; j < yNb / 2; j++) {
			GameObject go = Instantiate(dash, instantiationPosition, dash.transform.rotation, dashContainer.transform);
			go.transform.Rotate(transform.up, -90f);
			instantiationPosition.y += (spacing + width) * direction;
		}
		instantiationPosition.x += (spacing + width) / 2f;
		instantiationPosition.y = nextTransform.position.y;
		for (int j = 0; j < xNb / 2; j++) {

			Instantiate(dash, instantiationPosition, dash.transform.rotation, dashContainer.transform);

			instantiationPosition.x += spacing + width;

		}
	}
}
