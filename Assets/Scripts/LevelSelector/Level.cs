﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Level : MonoBehaviour {

	public Material unlockedMaterial;
	public Material lockedMaterial;
	public Material finishedMaterial;

	private MeshRenderer meshRenderer;
	private TextMeshPro bestTime;
	private TextMeshPro levelNameText;
	private TextMeshPro levelHintText;
	private SpriteRenderer answerSprite;
	private Status currentStatus = Status.Locked;

	public enum Status {
		Locked,
		Unlocked,
		Finished
	}

	private void Start() {
		meshRenderer = GetComponentInChildren<MeshRenderer>();
		TextMeshPro[] txtmp = GetComponentsInChildren<TextMeshPro>();
		bestTime = txtmp[0];
		levelNameText = txtmp[1];
		levelHintText = txtmp[2];
		answerSprite = GetComponentInChildren<SpriteRenderer>();
		answerSprite.enabled = false;
		SetState(Status.Locked);
	}

	public void SetState(Status status) {

		switch (status) {
			case Status.Unlocked:
				levelNameText.color = Color.white;
				levelNameText.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
				levelHintText.color = Color.white;
				levelHintText.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
				meshRenderer.material = unlockedMaterial;
				meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
				bestTime.text = "";
				if (GameManager.instance.cheatMode) {
					answerSprite.enabled = true;
				}
				break;
			case Status.Locked:
				levelNameText.color = Color.clear;
				levelNameText.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
				levelHintText.color = Color.clear;
				levelHintText.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
				meshRenderer.material = lockedMaterial;
				meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
				bestTime.text = "";
				break;
			case Status.Finished:
				levelNameText.color = Color.white;
				levelNameText.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
				levelHintText.color = Color.white;
				levelHintText.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
				meshRenderer.material = finishedMaterial;
				meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
				answerSprite.enabled = true;
				bestTime.text = "Time: " + Utils.FormatTime(GameManager.instance.savedLevelHashMap[gameObject.name].GetBestTime());
				break;
		}
	}

	public Status IsUnlocked() {
		return currentStatus;
	}

}
