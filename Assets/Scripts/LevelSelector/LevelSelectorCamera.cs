﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelSelectorCamera: MonoBehaviour {

	[Range(0f, 50f)]
	public float speed = 1f;
	[Range(0f, 20f)]
	public float dragSpeed = 10f;

	public bool followAnimation = true;

	private Vector3 targetPosition;

	private bool needToMove = false;
	private bool dragging = false;

	private Vector3 clickPosition;
	private Vector3 camOriginalPosition;

	void Start() {
		targetPosition = transform.position;
	}

    void Update() {
		//HandleMovement();
		HandleClick();
		SmoothTranslate();
		SmoothTranslateToNewPosition();
	}

	void LateUpdate() {
		Vector3 clampedPosition = transform.position;
		clampedPosition.x = Mathf.Clamp(transform.position.x, -30f, 30f);
		clampedPosition.y = Mathf.Clamp(transform.position.y, -30f, 30f);
		transform.position = clampedPosition;
	}

	//void HandleMovement() {
	//	Vector2 moveVector = Vector2.zero;

	//	moveVector.x = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
	//	moveVector.y = Input.GetAxis("Vertical") * speed * Time.deltaTime;

	//	if (moveVector.x != 0f || moveVector.y != 0f) {
	//		needToMove = false;
	//	}
	//	transform.Translate(moveVector);
	//}

	void HandleClick() {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;

			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)) {
				if (hit.transform.CompareTag("Level")) {
					clickPosition = hit.transform.position;
					clickPosition.z = transform.position.z;
					targetPosition = clickPosition;
					if (Vector3.Distance(transform.position, clickPosition) < 0.1f && (GameManager.instance.savedLevelHashMap[hit.transform.parent.name].GetStatus() != Level.Status.Locked) || GameManager.instance.cheatMode) {
						SceneManager.LoadScene(hit.transform.parent.name);
						GameManager.lastUnlockedLevel = null;
					} else {
						needToMove = true;
					}
				} else {
					clickPosition = Input.mousePosition;
					clickPosition.z = transform.position.z;
					camOriginalPosition = transform.position;
					dragging = true;
					needToMove = false;
					followAnimation = false;
				}
			}
		} else if (Input.GetMouseButtonUp(0)) {
			dragging = false;
			if (!needToMove) {
				targetPosition = transform.position;
			}
		}
	}

	void SmoothTranslate() {
		if (needToMove) {
			transform.position = Vector3.Lerp(transform.position, clickPosition, Time.deltaTime * speed);
			if (transform.position == clickPosition) {
				needToMove = false;
			}
		} else if (dragging) {

			RaycastHit hit;

			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)) {
				Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - clickPosition);
				pos = camOriginalPosition - (pos * dragSpeed);
				pos.z = transform.position.z;
				transform.position = pos;

			}


		}
	}

	void SmoothTranslateToNewPosition() {
		if (transform.position != targetPosition && !dragging && !needToMove) {
			transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * speed);
		}
	}

	public void SetTarget(Vector3 newTarget) {
		if (dragging) {
			return;
		}
		targetPosition = newTarget;
	}
}
