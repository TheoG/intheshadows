﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance = null;

	public GameObject background;
	public GameObject pauseMenu;
	public GameObject optionMenu;
	public Text endLevelText;
	public Text timer;

	public float time;
	public float timeInPause;
	public bool stopTimer = false;

	private Options options;

	public delegate void SetPause(bool pause);
	public static event SetPause setPause;

	private Button[] buttons;

	void Awake() {
		if (instance == null) {
			instance = this;
		}
	}

	void Start() {
		time = 0;
		timeInPause = 0;
		buttons = pauseMenu.GetComponentsInChildren<Button>();

		buttons[0].onClick.AddListener(Resume);
		buttons[1].onClick.AddListener(RestartLevel);
		buttons[2].onClick.AddListener(Options);
		buttons[3].onClick.AddListener(QuitLevel);

		foreach (Button b in buttons) {
			b.onClick.AddListener(SoundManager.instance.ClickButton);
		}

		options = optionMenu.GetComponent<Options>();

		background.SetActive(false);
		pauseMenu.SetActive(false);
		optionMenu.SetActive(false);
		InvokeRepeating("UpdateTimer", 1f, 1f);
	}

	void Update() {
		if (!stopTimer) {
			time = Time.timeSinceLevelLoad - timeInPause;
		}
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (optionMenu.activeSelf) {
				CloseOptions();
			} else {
				CloseMenu();
			}
		}
		if (background.activeSelf) {
			timeInPause += Time.deltaTime;
		}
	}

	void UpdateTimer() {
		timer.text = Utils.FormatTime(time);
	}

	public void CloseMenu() {
		background.SetActive(!background.activeSelf);
		pauseMenu.SetActive(!pauseMenu.activeSelf);

		setPause(background.activeSelf);
		if (!ShadowObjectManager.instance.IsEndOfLevel()) {
			stopTimer = background.activeSelf;
		}

		if (background.activeSelf) {
			endLevelText.gameObject.SetActive(false);
		}
	}

	public void CloseOptions() {
		optionMenu.SetActive(false);
		pauseMenu.SetActive(true);
	}

	public void Resume() {
		CloseMenu();
	}

	public void Options() {

		pauseMenu.SetActive(false);
		optionMenu.SetActive(true);
		options.RefreshValues();

	}

	public void RestartLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void QuitLevel() {
		SceneManager.LoadScene(1);
	}
}
