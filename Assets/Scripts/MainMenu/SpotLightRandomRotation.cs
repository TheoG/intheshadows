﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotLightRandomRotation : MonoBehaviour {

	[Range(0f, 100f)]
	public float speed = 1;
	[Range(0.01f, 360f)]
	public float amplitude = 10;

	private Vector3 rotation;

	void Start() {
		rotation = transform.rotation.eulerAngles;
	}

	void Update() {

		rotation.y = Mathf.Sin(Time.timeSinceLevelLoad * speed) * (amplitude / 2);
		transform.rotation = Quaternion.Euler(rotation);

		//transform.Rotate(Vector3.right, Mathf.Cos(Time.time / 5f) / 20f);

	}
}
