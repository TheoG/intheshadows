﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MainMenu : MonoBehaviour {

	private List<Button> buttons = new List<Button>();

	void Start() {

		buttons.AddRange(GetComponentsInChildren<Button>());
		
		if (!GameManager.instance.HasSave()) {
			buttons[0].gameObject.SetActive(false);
			buttons[1].transform.parent.GetComponent<RectTransform>().position = buttons[1].transform.parent.GetComponent<RectTransform>().position + new Vector3(0f, 10f, 0f);
		}

		buttons[0].onClick.AddListener(GameManager.instance.LevelSelector);
		buttons[1].onClick.AddListener(GameManager.instance.LevelSelectorNewGame);
		buttons[2].onClick.AddListener(GameManager.instance.CheatMode);
		buttons[3].onClick.AddListener(GameManager.instance.DisplayOptions);
		buttons[4].onClick.AddListener(GameManager.instance.QuitGame);


		foreach (Button b in buttons) {
			b.onClick.AddListener(SoundManager.instance.ClickButton);
		}
	}

    void Update() {
        
    }

}
