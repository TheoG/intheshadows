﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitingLights : MonoBehaviour {

	public Vector3 axisPoint;
	public float speed = 10f;

	void Start() {
	   
	}

    void Update() {
		transform.RotateAround(axisPoint, transform.forward, Time.deltaTime * speed);
	}
}
