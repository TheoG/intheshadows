﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCamera : MonoBehaviour {

	[HideInInspector]
	public Vector3 eulerTarget;
	public float speed;

	private Vector3 refRot;

	void Start () {
		eulerTarget = Vector3.zero;
	}
	
	void Update () {
		if (transform.eulerAngles != eulerTarget) {
			transform.eulerAngles = Vector3.SmoothDamp(transform.eulerAngles, eulerTarget, ref refRot, speed);
		}
	}
}
