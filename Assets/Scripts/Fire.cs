﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour {

	public float changeSpeed = 1f;
	public float minIntensity = 1f;
	public float maxIntensity = 2f;

	private float refSpeed = 0f;
	private float intensity;
	private Light lightComponent;

	void Start () {
		InvokeRepeating("RandomIntensity", 0, changeSpeed);
		lightComponent = GetComponent<Light>();

	}
	
	void Update () {
		lightComponent.intensity = Mathf.SmoothDamp(lightComponent.intensity, intensity, ref refSpeed, changeSpeed);
	}

	void RandomIntensity() {
		intensity = Random.Range(minIntensity, maxIntensity);
	}
}
