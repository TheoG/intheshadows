﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance = null;

	public List<AudioClip> soundtracks;
	public AudioClip buttonClick;
	public AudioClip winningSfx;
	public AudioClip particleSfx;

	public AudioSource mainTrackSource;
	public AudioSource sfx;
	private int lastTrack;

	void Awake() {
		if (instance == null) {
			instance = this;
		}
	}

	void Start () {
		mainTrackSource = gameObject.AddComponent<AudioSource>();
		mainTrackSource.priority = 0;
		lastTrack = Random.Range(0, soundtracks.Count);
		mainTrackSource.clip = soundtracks[lastTrack];
		mainTrackSource.Play();

		sfx = gameObject.AddComponent<AudioSource>();
		StartCoroutine(NextTrack());

		mainTrackSource.volume = Preferences.values[Preferences.musicVolumeIndex] / 100f;
		sfx.volume = Preferences.values[Preferences.sfxVolumeIndex] / 100f;
	}

	public void ClickButton() {
		sfx.PlayOneShot(buttonClick);
	}

	public void Winning() {
		sfx.PlayOneShot(winningSfx);
	}

	public void WinningParticle() {
		sfx.PlayOneShot(particleSfx);
	}

	IEnumerator NextTrack() {
		while (true) {
			yield return new WaitForSeconds(mainTrackSource.clip.length + 2f);
			int newTrack = -1;
			do {
				newTrack = Random.Range(0, soundtracks.Count);
			} while (newTrack == lastTrack);
			lastTrack = newTrack;
			mainTrackSource.clip = soundtracks[lastTrack];
			mainTrackSource.Play();
		}
	}

	void Update () {
		
	}
}
