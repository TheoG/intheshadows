﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ShadowObject : MonoBehaviour {

	public float sensibility;
	public Collider solutionCollider;
	public Vector3 realRotation;

	private Vector3 clickPosition;
	private bool clicked = false;
	private List<SphereCollider> colliders = new List<SphereCollider>();
	private int nbCollisions = 0;
	private bool endOfLevel = false;
	private bool pause = false;
	private bool correct;

	private Vector3 originalEulerAngles;
	private bool canVerticalRotate = false;

	private Quaternion realRotationQuaternion;

	private float speed = 30f;

    void Start() {
		colliders.AddRange(GetComponents<SphereCollider>());
		sensibility = Preferences.values[Preferences.sensibilityIndex];
		canVerticalRotate = char.GetNumericValue(SceneManager.GetActiveScene().name.Substring(6)[0]) > 1;

		realRotationQuaternion = Quaternion.Euler(realRotation);
	}

	void OnEnable() {
		Options.onSettingChange += UpdateSettings;
		LevelManager.setPause += Pause;
	}

	void OnDisable() {
		Options.onSettingChange -= UpdateSettings;
		LevelManager.setPause -= Pause;
	}

	void UpdateSettings(string index, float value) {
		if (index.Equals(Preferences.sensibilityIndex)) {
			sensibility = value;
		}
	}

	public void Pause(bool pause) {
		this.pause = pause;
	}

	void Update() {
		if (pause) return;

		HandleClick();
		HandleMovements();
		HandleEndOfLevel();
		HandleDevMode();
	}
	
	private void HandleClick() {
		if (endOfLevel) {
			if (transform.eulerAngles != realRotation) {
				transform.rotation = Quaternion.RotateTowards(transform.rotation, realRotationQuaternion, speed * Time.deltaTime);
			}
			return;
		}

		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;

			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, LayerMask.GetMask("ShadowObject"))) {
				if (hit.transform.CompareTag("Player") && hit.transform.gameObject == gameObject) {
					clickPosition = Input.mousePosition;
					clicked = true;
				}
			}
		}
		if (Input.GetMouseButtonUp(0)) {
			clicked = false;
		}
	}

	private void HandleMovements() {
		Vector3 rotations = Vector3.zero;

		if (!endOfLevel && ShadowObjectManager.instance.parts.Count > 1 && Input.GetKey(KeyCode.LeftShift) && clicked) {
			transform.position = new Vector3(transform.position.x, transform.position.y - ((clickPosition.y - Input.mousePosition.y) / Screen.height) * (sensibility / 40f), transform.position.z);
		} else if (clicked && !endOfLevel) {
			if (Input.GetKey(KeyCode.LeftControl) && canVerticalRotate) {
				rotations.x = -((clickPosition.y - Input.mousePosition.y) / Screen.height) * sensibility;
			} else {
				rotations.y = ((clickPosition.x - Input.mousePosition.x) / Screen.width) * sensibility;
				rotations.z = -((clickPosition.y - Input.mousePosition.y) / Screen.height) * sensibility;
			}
			transform.Rotate(rotations, Space.World);
		}

	}

	private void HandleDevMode() {
		if (Input.GetKeyDown(KeyCode.N)) {
			transform.eulerAngles = realRotation;
		}
	}

	private void HandleEndOfLevel() {

		if (nbCollisions == colliders.Count + 1 && !correct) {
			correct = true;
		} else if (correct) {
			correct = false;
		}
	}

	private void OnTriggerEnter(Collider other) {
		nbCollisions++;
	}

	private void OnTriggerExit(Collider other) {
		nbCollisions--;
	}

	public bool IsCorrect() {
		return correct;
	}

	public void SetEndOfLevel(bool endOfLevel) {
		this.endOfLevel = endOfLevel;
	}
}
