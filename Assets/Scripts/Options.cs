﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Options : MonoBehaviour {

	private Slider sensibilitySlider;
	private Text sensibilityValue;

	private Slider musicVolumeSlider;
	private Text musicVolumeValue;

	private Slider sfxVolumeSlider;
	private Text sfxVolumeValue;

	private Button applyModificationButton;
	private Button quitButton;

	public delegate void OnSettingChange(string field, float value);
	public static event OnSettingChange onSettingChange;

	void Start() {
		sensibilitySlider = GameObject.Find("O_Sensibility_S").GetComponent<Slider>();
		sensibilitySlider.onValueChanged.AddListener(SensibilityChanged);
		sensibilityValue = GameObject.Find("O_Sensibility_Value").GetComponent<Text>();

		musicVolumeSlider = GameObject.Find("O_MusicVolume_S").GetComponent<Slider>();
		musicVolumeSlider.onValueChanged.AddListener(MusicVolumeChanged);
		musicVolumeValue = GameObject.Find("O_MusicVolume_Value").GetComponent<Text>();

		sfxVolumeSlider = GameObject.Find("O_SfxVolume_S").GetComponent<Slider>();
		sfxVolumeSlider.onValueChanged.AddListener(SfxVolumeChanged);
		sfxVolumeValue = GameObject.Find("O_SfxVolume_Value").GetComponent<Text>();


		applyModificationButton = GameObject.Find("O_Apply_B").GetComponent<Button>();
		applyModificationButton.onClick.AddListener(SaveChange);
		applyModificationButton.onClick.AddListener(SoundManager.instance.ClickButton);

		quitButton = GameObject.Find("O_Quit_B").GetComponent<Button>();
		quitButton.onClick.AddListener(Quit);
		quitButton.onClick.AddListener(SoundManager.instance.ClickButton);
		RefreshValues();
	}
	
	void OnEnable() {

	}

	void OnDisable() {

	}

	public void RefreshValues() {
		sensibilitySlider.value = Preferences.values[Preferences.sensibilityIndex];
		SensibilityChanged(Preferences.values[Preferences.sensibilityIndex]);
		musicVolumeSlider.value = Preferences.values[Preferences.musicVolumeIndex];
		MusicVolumeChanged(Preferences.values[Preferences.musicVolumeIndex]);
		sfxVolumeSlider.value = Preferences.values[Preferences.sfxVolumeIndex];
		SfxVolumeChanged(Preferences.values[Preferences.sfxVolumeIndex]);
	}


	// Events

	public void SaveChange() {
		Preferences.SetFloat(Preferences.sensibilityIndex, sensibilitySlider.value);
		Preferences.SetFloat(Preferences.musicVolumeIndex, musicVolumeSlider.value);
		Preferences.SetFloat(Preferences.sfxVolumeIndex, sfxVolumeSlider.value);
		if (onSettingChange != null) {
			onSettingChange(Preferences.sensibilityIndex, sensibilitySlider.value);
		}
		SoundManager.instance.mainTrackSource.volume = musicVolumeSlider.value / 100f;
		SoundManager.instance.sfx.volume = sfxVolumeSlider.value / 100f;
	}

	public void Quit() {
		if (SceneManager.GetActiveScene().buildIndex == 0) {
			GameManager.instance.QuitOptions();
		} else {
			LevelManager.instance.CloseOptions();
		}
	}

	public void SensibilityChanged(float value) {
		sensibilityValue.text = ((int)value).ToString();
	}

	public void MusicVolumeChanged(float value) {
		musicVolumeValue.text = ((int)value).ToString();
	}

	public void SfxVolumeChanged(float value) {
		sfxVolumeValue.text = ((int)value).ToString();
	}

}
