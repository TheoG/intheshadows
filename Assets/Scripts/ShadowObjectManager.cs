﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShadowObjectManager : MonoBehaviour {

	public List<ShadowObject> parts;
	public Vector3 centerPoint = Vector3.zero;

	public static ShadowObjectManager instance = null;

	private bool endOfLevel = false;
	private Animator lightAnimator;

	private void Awake() {
		if (instance == null) {
			instance = this;
		}
	}

	private void Start() {
		lightAnimator = GameObject.Find("Main Spot Light").GetComponent<Animator>();

		foreach (ShadowObject shadowObject in parts) {
			centerPoint += shadowObject.transform.position;
		}
		centerPoint /= parts.Count;
	}

	private void Update() {

		if (endOfLevel) {
			if (Input.GetKeyDown(KeyCode.Return)) {
				SceneManager.LoadScene(1);
			}
			return;
		}


		bool done = true;

		foreach (ShadowObject shadowObject in parts) {
			if (!shadowObject.IsCorrect()) {
				done = false;
				break;
			}
		}

		if (done) {
			endOfLevel = true;
			foreach (ShadowObject shadowObject in parts) {
				shadowObject.SetEndOfLevel(true);
			}
			HandleEndOfLevel();
		}
	}

	public bool IsEndOfLevel() {
		return endOfLevel;
	}

	IEnumerator DisplayEndLevelMessage() {
		yield return new WaitForSeconds(1f);

		while (LevelManager.instance.endLevelText.color.a < 1f) {
			LevelManager.instance.endLevelText.color = new Color(
				LevelManager.instance.endLevelText.color.r,
				LevelManager.instance.endLevelText.color.g,
				LevelManager.instance.endLevelText.color.b,
				LevelManager.instance.endLevelText.color.a + 0.005f);
			yield return new WaitForSeconds(0.01f);
		}
	}

	private void HandleEndOfLevel() {
		string sceneName = SceneManager.GetActiveScene().name;

		lightAnimator.SetTrigger("WinningLight");
		SoundManager.instance.Winning();
		LevelManager.instance.stopTimer = true;

		StartCoroutine(DisplayEndLevelMessage());

		LevelManager.instance.endLevelText.gameObject.SetActive(true);

		if (!GameManager.instance.cheatMode) {
			SavedLevel savedLevel = GameManager.instance.savedLevelHashMap[sceneName];
			if (savedLevel.GetBestTime() == -1 || (savedLevel.GetBestTime() > 0 && savedLevel.GetBestTime() > Mathf.RoundToInt(Time.timeSinceLevelLoad))) {
				savedLevel.SetBestTime(Mathf.RoundToInt(Time.timeSinceLevelLoad));
			}

			List<string> nextLevels = savedLevel.GetNextLevel();
			bool newUnlock = true;
			if (nextLevels != null) {
				foreach (string nextLevel in nextLevels) {
					if (GameManager.instance.savedLevelHashMap[nextLevel].GetStatus() == Level.Status.Locked) {
						GameManager.instance.savedLevelHashMap[nextLevel].SetStatus(Level.Status.Unlocked);
					} else {
						newUnlock = false;
					}
				}
			}
			if (newUnlock) {
				GameManager.lastUnlockedLevel = GameManager.instance.savedLevelHashMap[sceneName];
			} else {
				GameManager.lastUnlockedLevel = null;
			}
			savedLevel.SetFinished();
			GameManager.instance.SaveProgression();
		}

	}
}
