﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour {

	public static string FormatTime(float time) {
		float minutes = Mathf.Floor(time / 60);
		float seconds = Mathf.RoundToInt(time % 60);

		string finalString = "";

		if (minutes < 10) {
			finalString += "0";
		}
		finalString += minutes.ToString() + ":";

		if (seconds < 10) {
			finalString += "0";
		}
		finalString += seconds.ToString();

		return finalString;
	}

}
