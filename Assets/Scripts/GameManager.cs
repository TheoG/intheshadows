﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;

	public bool cheatMode = false;
	private List<SavedLevel> savedLevel = new List<SavedLevel>();
	public Dictionary<string, SavedLevel> savedLevelHashMap = new Dictionary<string, SavedLevel>();

	private string pathToSave;
	private bool hasSave;

	private MainMenuCamera mainMenuCamera;

	[HideInInspector]
	public static SavedLevel lastUnlockedLevel;

    void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);
	}

	void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	public void Start() {
		pathToSave = Path.Combine(Application.persistentDataPath, "save.its");
		LoadProgression();
		GameManager.lastUnlockedLevel = null;
	}

	public void MainMenu() {
		GameManager.lastUnlockedLevel = null;
		SceneManager.LoadScene(0);
	}

	public void LevelSelector() {
		cheatMode = false;
		SceneManager.LoadScene(1);
	}

	public void CheatMode() {
		cheatMode = true;
		SceneManager.LoadScene(1);
	}

	public void LevelSelectorNewGame() {
		cheatMode = false;
		File.Delete(pathToSave);
		LoadProgression();
		SceneManager.LoadScene(1);
	}

	public void DisplayOptions() {
		if (mainMenuCamera) {
			mainMenuCamera.eulerTarget = new Vector3(61f, 0f, 0f);
		}
	}

	public void QuitOptions() {
		if (mainMenuCamera) {
			mainMenuCamera.eulerTarget = Vector3.zero;
		}
	}

	public void QuitGame() {
		Application.Quit();
	}

	public void SaveProgression() {
		savedLevel = new List<SavedLevel>(savedLevelHashMap.Values);
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(pathToSave);
		bf.Serialize(file, savedLevel);
		file.Close();
		hasSave = true;
	}

	// TODO: check for corrumpted files
	public void LoadProgression() {

		savedLevel.Clear();
		savedLevelHashMap.Clear();

		if (File.Exists(pathToSave)) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(pathToSave, FileMode.Open);
			savedLevel = (List<SavedLevel>)bf.Deserialize(file);
			file.Close();
		} else {
			LoadNewGame();
		}

		foreach (SavedLevel sl in savedLevel) {
			savedLevelHashMap.Add(sl.GetName(), sl);
		}
		hasSave = savedLevel[0].GetStatus() == Level.Status.Finished;
	}

	public void LoadNewGame() {
		savedLevel.Add(new SavedLevel("Level_1-1", "British", -1, Level.Status.Unlocked, new List<string>(new string[] { "Level_2-1" })));

		savedLevel.Add(new SavedLevel("Level_2-1", "Fat", -1, Level.Status.Locked, new List<string>(new string[] { "Level_2-2" })));
		savedLevel.Add(new SavedLevel("Level_2-2", "AnonyMiaous", -1, Level.Status.Locked, new List<string>(new string[] { "Level_3-1" })));
		savedLevel.Add(new SavedLevel("Level_3-1", "Flat", -1, Level.Status.Locked, new List<string>(new string[] { "Level_3-2" })));
		savedLevel.Add(new SavedLevel("Level_3-2", "The Answer", -1, Level.Status.Locked, null));
	}

	public bool HasSave() {
		return hasSave;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		// We are loading the lvl selector scene
		if (scene.buildIndex == 1) {
			GameObject backArrow = GameObject.Find("BackMainMenu");
			if (backArrow) {
				backArrow.GetComponent<Button>().onClick.AddListener(MainMenu);
				backArrow.GetComponent<Button>().onClick.AddListener(SoundManager.instance.ClickButton);
			}
		} else if (scene.buildIndex == 0) {
			mainMenuCamera = GameObject.Find("Main Camera").GetComponent<MainMenuCamera>();
		}
	}
}

[System.Serializable]
public class SavedLevel {
	private string name;
	private string hint;
	private int bestTime;
	private Level.Status status;
	private List<string> nextLevels;

	public SavedLevel(string name, string hint, int bestTime, Level.Status status, List<string> nextLevels) {
		this.name = name;
		this.hint = hint;
		this.bestTime = bestTime;
		this.status = status;
		this.nextLevels = nextLevels;
	}

	public string GetName() {
		return name;
	}

	public string GetHint() {
		return hint;
	}

	public int GetBestTime() {
		return bestTime;
	}

	public void SetStatus(Level.Status status) {
		this.status = status; 
	}

	public void SetFinished() {
		this.status = Level.Status.Finished;
	}

	public void SetBestTime(int seconds) {
		this.bestTime = seconds;
	}

	public List<string> GetNextLevel() {
		return this.nextLevels;
	}

	public Level.Status GetStatus() {
		return status;
	}
}
